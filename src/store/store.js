import Vue from "vue"
import Vuex from "vuex"
import auth from "./modules/auth";
import goal from './modules/goal';

Vue.use(Vuex)

export default new Vuex.Store({
    modules : {
        auth,
        goal,
    }
})
