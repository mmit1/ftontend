import {allGoalsFake, userGoalsFake} from "@/fakeData";

export default {
    actions: {
        async createGoal(ctx, goal = {}) {
            const formData = new FormData();
            formData.append('title', goal.title);
            formData.append('description', goal.description);
            formData.append('dateStart', goal.dateStart);
            formData.append('dateEnd', goal.dateEnd);
            formData.append('bet', goal.bet);
            formData.append('privateStatus', goal.privateStatus);
            goal.photos.forEach((photo, i)=>{
                formData.append(`photo${i}`, photo, photo.name);
            });
            // for (var pair of formData.entries()) {
            //     console.log(pair[0]+ ', ' + pair[1]);
            // }
            await fetch('http://localhost:8000/goal/create', {
                method: 'POST',
                credentials: 'include',
                body: formData
            })
            // if (resp.ok) {
            //
            // }
            // const posts = await resp.json();
            // ctx.commit('updatePosts', posts)
        },

        // async getAllGoals(ctx, goal = {}) {
        //     const formData = new FormData();
        //     formData.append('title', goal.title);
        //     formData.append('description', goal.description);
        //     const resp = await fetch('http://localhost:8000/goals', {
        //         credentials: 'include',
        //     })
        //     const posts = await resp.json();
        //     ctx.commit('updatePosts', posts)
        // }

    },
    mutations: {
        enterGoal(state, id) {
            state.userGoals.push(state.allGoals.filter(g => g.id === id)[0])
            state.allGoals = state.allGoals.filter(g => g.id !== id)
        },
    },
    state: {
        allGoals: allGoalsFake,
        userGoals: userGoalsFake,
    },
    getters: {
        allGoals(state) {
            return state.allGoals
        },
        userGoals(state) {
            return state.userGoals
        }
    },
}