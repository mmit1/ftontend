export default {
    actions: {
        async register(ctx, userData) {
            console.log("dispatch OK")
            fetch("http://localhost:8000/register", {
                method: "POST",
                body: JSON.stringify(userData)
            }).then((response) => {
                if (response.status === 200) {
                    ctx.commit('registerSuccess')
                } else {
                    //TODO:Передавать ошибку с бека
                    ctx.commit('registerError', "internal Error")
                }
            }).catch((response) => {
                ctx.commit('registerError', response)
            })
        },
        async login(ctx, userData) {
            console.log(userData)
            fetch("http://localhost:8000/login", {
                method: "POST",
                body: JSON.stringify(userData)
            })
                .then((response) => {
                    response.json().then((parsedBody) => {
                        if (response.status === 200) {
                            console.log(parsedBody)
                            ctx.commit('authSuccess', parsedBody)
                        } else {
                            ctx.commit('authError', parsedBody)
                        }
                    }).catch(() => {
                        ctx.commit('authError', "internal Error")
                    })
                })
                .catch((response) => {
                    ctx.commit('authError', response)
                })
        }
    },
    mutations: {
        authSuccess(state, token) {
            state.auth = true
            state.authError = null
            state.token = token
        },
        authError(state, error) {
            state.authError = error
        },
        registerSuccess(state) {
            state.register = true
            state.registerError = null
        },
        registerError(state, error) {
            state.registerError = error
        }
    },
    state: {
        token: "",
        auth: false,
        authError: null,

        register: false,
        registerError: null
    },
    getters: {
        isAuth(state) {
            return state.auth
        },
        getAuthError(state) {
            return state.authError
        },
        isRegister(state) {
            return state.register
        },
        getRegisterError(state) {
            return state.registerError
        },
        getToken(state) {
            return state.token
        }
    }
}
