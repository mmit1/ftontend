import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
// Require dependencies
let VueCookie = require('vue-cookie');
// Tell Vue to use the plugin
Vue.use(VueCookie);

let router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/register',
            component: () => import('./views/Register')
        },
        {
            path: '/login',
            component: () => import('./views/Login')
        },
        {
            path: '/goals',
            component: () => import('./views/AllGoals'),
            meta: {
                requiresAuth : true
            }
        },
        {
            path: '/goals/my',
            component: () => import('./views/UserGoals.vue'),
            meta: {
                requiresAuth : true
            }
        },
        {
            path: '/goals/end',
            component: () => import('./views/ClosedGoals'),
            meta: {
                requiresAuth : true
            }
        },
        {
            path: '/goal/create',
            component: () => import('./views/NewGoal'),
            meta: {
                requiresAuth : true
            }
        }
    ]
})

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (VueCookie.get('session_id') == null) {
            next({
                path: '/login',
            })
        } else {
            next()
        }
    } else {
        next()
    }
})

export default router
