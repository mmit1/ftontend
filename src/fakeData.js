export const allGoalsFake = [
    {
        id: 0,
        title: 'Каждый день бегать в 6 утра',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem ea quos saepe! Aspernatur autem distinctio eius error in ipsa iure nesciunt ratione repellendus saepe! Beatae dolore harum iure quis quod.',
        tags: [
            {color: 'red', name: 'Спорт'},
            {color: 'green', name: 'Мотивация'},
        ],
        startDate: '19.05.2001',
        endDate: '31.05.2001',
        photos: [
            {
                src: 'https://i.pinimg.com/564x/77/64/41/7764419616c64d29426fbbe8e31cef58.jpg',
            },
            {
                src: 'https://cdn.vuetifyjs.com/images/carousel/sky.jpg',
            },
            {
                src: 'https://cdn.vuetifyjs.com/images/carousel/bird.jpg',
            },
            {
                src: 'https://cdn.vuetifyjs.com/images/carousel/planet.jpg',
            },
        ],
    },
    {
        id: 1,
        title: 'Закрыться до лета',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem ea quos saepe! Aspernatur autem distinctio eius error in ipsa iure nesciunt ratione repellendus saepe! Beatae dolore harum iure quis quod.',
        tags: [
            {color: 'blue', name: 'Учёба'},
            {color: 'yellow', name: 'Характер'},
        ],
        startDate: '31.05.2021',
        endDate: '31.08.2021',
        photos: [
            {
                src: 'https://static.dw.com/image/16130849_303.jpg',
            },
            {
                src: 'https://news.utmn.ru/upload/iblock/06d/onlaynuchebastalaintensivnee.jpg',
            }
        ],
    },
    {
        id: 2,
        title: 'Похудеть до 76кг',
        text: 'Жиры в диетологии — один из важнейших компонентов пищи наряду с белками и углеводами. Химически представляют собой липиды[1].\n' +
            '\n' +
            'В организме человека основную часть жиров составляют триглицериды. Кроме них жирами называют фосфолипиды, стерины (в том числе холестерин). Жиры играют в организме роль источника энергии и составляют около 80 % её запасов[2]. Помимо этого, в составе липопротеидов, жиры выполняют функцию строительного материала клеток[3]. Общепринято деление пищевых липидов по их агрегатному состоянию при комнатной температуре: твёрдые вещества — собственно, жиры; жидкие вещества — масла.\n' +
            '\n' +
            'Жирные кислоты, содержащиеся в жирах, подразделяются на насыщенные и ненасыщенные. Источником насыщенных жирных кислот является пища животного происхождения, ненасыщенных — растительного. Также насыщенные и мононенасыщенные жирные кислоты могут синтезироваться в организме[2].',
        tags: [
            {color: 'red', name: 'Спорт'},
            {color: 'pink', name: 'Похудение'},
        ],
        startDate: '20.05.2021',
        endDate: '20.06.2021',
        photos: [
            {
                src: 'https://kor.ill.in.ua/m/610x385/1559379.jpg',
            },
            {
                src: 'https://1gai.ru/uploads/posts/2020-07/1593762533_221122.jpg',
            }
        ],
    },
    {
        id: 3,
        title: 'Пойти погулять',
        text: '',
        tags: [
            {color: 'blue', name: 'Досуг'},
            {color: 'red', name: 'Спорт'},
            {color: 'blue', name: 'Досуг'},
            {color: 'red', name: 'Спорт'},
            {color: 'blue', name: 'Досуг'},
            {color: 'red', name: 'Спорт'},
            {color: 'red', name: 'Спорт'},
            {color: 'blue', name: 'Досуг'},
            {color: 'red', name: 'Спорт'},
        ],
        startDate: '20.05.2021',
        endDate: '20.06.2021',
        photos: [
        ],
    },
]

export const userGoalsFake = [
    {
        id: 0,
        title: 'Каждый день бегать в 6 утра',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem ea quos saepe! Aspernatur autem distinctio eius error in ipsa iure nesciunt ratione repellendus saepe! Beatae dolore harum iure quis quod.',
        tags: [
            {color: 'red', name: 'Спорт'},
            {color: 'green', name: 'Мотивация'},
        ],
        startDate: '19.05.2001',
        endDate: '31.05.2001',
        photos: [
            {
                src: 'https://i.pinimg.com/564x/77/64/41/7764419616c64d29426fbbe8e31cef58.jpg',
            },
            {
                src: 'https://cdn.vuetifyjs.com/images/carousel/sky.jpg',
            },
            {
                src: 'https://cdn.vuetifyjs.com/images/carousel/bird.jpg',
            },
            {
                src: 'https://cdn.vuetifyjs.com/images/carousel/planet.jpg',
            },
        ],
    },
    {
        id: 1,
        title: 'Закрыться до лета',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem ea quos saepe! Aspernatur autem distinctio eius error in ipsa iure nesciunt ratione repellendus saepe! Beatae dolore harum iure quis quod.',
        tags: [
            {color: 'blue', name: 'Учёба'},
            {color: 'yellow', name: 'Характер'},
        ],
        startDate: '31.05.2021',
        endDate: '31.08.2021',
        photos: [
            {
                src: 'https://static.dw.com/image/16130849_303.jpg',
            },
            {
                src: 'https://news.utmn.ru/upload/iblock/06d/onlaynuchebastalaintensivnee.jpg',
            }
        ],
    }
]